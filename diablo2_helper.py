#!/bin/python3

import time
import sys

# Install these modules using pip (ex: pip install pyautogui)
import pyautogui
import keyboard

#
# Settings
#

configs = {
    'default': { # This config is loaded if no argument is passed when running the script
        'left_click_keys' : '',
        'right_click_keys' : 'qwertf' # or [ 'f1', 'f2', 'f3', 'f4', 'f5', 'ctrl', 'space' ] if using non alphabetic keys
    },
    '0': { # This config will be loaded if the argument '0' is passed (ex: python3 diablo2_helper.py 0) 
        'left_click_keys' : 'qwertf',
        'right_click_keys' : ''
    },
}

# Set this as the key to hold the character position in-game (default is shift)
hold_position_key = 'shift'

# Set this interval to the prefered clicking interval (in seconds)
spam_interval = 0.01

#
#
#

# Load config
if len(sys.argv) > 1:
    if sys.argv[1] in configs:
        config = configs[sys.argv[1]]
        left_click_keys = config['left_click_keys']
        right_click_keys = config['right_click_keys']
        print("Running with config '{}'".format(sys.argv[1]))
    else:
        print("The selected config '{}' does not exist, reverting to default".format(sys.argv[1]))
else:
    config = configs['default']
    left_click_keys = config['left_click_keys']
    right_click_keys = config['right_click_keys']
    print("Running with default config")

# Detect loop
while True:
    # Pause when alt+tab is pressed, enter to resume
    if keyboard.is_pressed('alt') and keyboard.is_pressed('tab'):
        input("\nDetected alt+tab, script paused. Press Enter to resume")
        print("\nIt's running !")

    # Left click keys
    for key in left_click_keys:
        try:
            if keyboard.is_pressed(key):
                pyautogui.mouseUp(button='left')
                keyboard.press(hold_position_key)
                print("Pressing left click with key '{}'".format(key))
                click_count = 1
                while True:
                    pyautogui.click(button='left')
                    print("click #{}".format(click_count))
                    click_count += 1
                    time.sleep(spam_interval)
                    if not keyboard.is_pressed(key):
                        break
                keyboard.release(hold_position_key)
                print("Key '{}' released, stopped left clicking".format(key))
        except Exception as e:
            print(e)

    # Right click keys
    for key in right_click_keys:
        try:
            if keyboard.is_pressed(key):
                pyautogui.mouseUp(button='right')
                keyboard.press(hold_position_key)
                print("Pressing right click with key '{}'".format(key))
                click_count = 1
                while True:
                    pyautogui.click(button='right')
                    print("click #{}".format(click_count))
                    click_count += 1
                    time.sleep(spam_interval)
                    if not keyboard.is_pressed(key):
                        break
                keyboard.release(hold_position_key)
                print("Key '{}' released, stopped right clicking".format(key))
        except Exception as e:
            print(e)

    time.sleep(0.001)
