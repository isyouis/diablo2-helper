# Introduction

Like playing Diablo 2 but your hand hurts because of all the clicking ? This script is for you. It makes it so pressing a skill key will also make your character use it straight away by either left clicking or right clicking automatically for you. The result is the same as using skills in games like League of Legends, Diablo 3, Path of Exile, etc.

# Prerequisites

Ensure you have python3 installed if you are on Windows and ensure that python is added to your path variable (check the checkbox when installing), as well as the following python modules :
```
pyautogui
keyboard
```

You can install these modules with pip (in Windows after python is installed, open cmd and enter the following commands)
```
pip install pyautogui
pip install keyboard
```

## For Linux users

Ensure these pip modules are installed into your root user's python3 install and that the script is ran as root. To do this simply run the pip commands and the script as root with sudo.

Pyautogui requires that tk is installed, you can install it with these commands

Aptitude (Ubuntu, Mint, etc.) :
```
sudo apt install python3-tk
```

Pacman (Arch) :
```
sudo pacman -S tk
```

Then install the python packages with sudo
```
sudo pip install pyautogui
sudo pip install keyboard
```

You may now download diablo2_helper.py

# Configuration

You can edit which keys are used by simply editing the script with any text editor and changing the config variable dictionnary.

In-game, check which skills are bound to what buton. Skills bound to the left action button (left click) should be listed under left_click_keys and the skills bound to the right action button (right click) shound be listed under right_click_keys. You may have to change the script's settings depending on your build.

Example:
```
configs = {
  'default' = {
    'left_click_keys' : '',
    'right_click_keys' : [ 'f1', 'f2', 'f3', 'f4', 'f5' ]
  },
  'paladin' = {
    'left_click_keys' : [ 'f1', 'f2', 'f3', 'f4', 'f5' ],
    'right_click_keys' : ''
  }
}
```

Ensure that the key you bound to the "hold position" action in-game is set to the same key in the script (shift is the default)
```
hold_position_key = 'shift'
```

# Usage

Simply run the python script, then open your game.

## Windows

Create a batch file (.bat) in the same directory as the script with the following line (or run following line in cmd) and run it:
```
python3 diablo2_helper.py
```

## Linux
```
sudo ./diablo2_helper.py
```

or

```
sudo python3 diablo2_helper.py
```

Don't forget to exit the script after you're finished playing ;)

# How it works

When you press one of the keys, the script presses the "hold position" key, then presses either right click or left click repeatedly until the skill key is released at which point the "hold position" key is also released.
